state_data = {
    "MD": {
        "candidate": "none",
        "marij_law": "medical"
    },
    "DE": {
        "candidate": "none",
        "marij_law": "medical"
    },
    "NJ": {
        "candidate": "none",
        "marij_law": "medical"
    },
    "NY": {
        "candidate": "none",
        "marij_law": "medical"
    },
    "CT": {
        "candidate": "none",
        "marij_law": "medical"
    },
    "RI": {
        "candidate": "none",
        "marij_law": "medical"
    },
    "MA": {
        "candidate": "clinton",
        "marij_law": "medical"
    },
    "ME": {
        "candidate": "sanders",
        "marij_law": "medical"
    },
    "NH": {
        "candidate": "sanders",
        "marij_law": "medical"
    },
    "VT": {
        "candidate": "sanders",
        "marij_law": "medical"
    },
    "MI": {
        "candidate": "sanders",
        "marij_law": "medical"
    },
    "IL": {
        "candidate": "clinton",
        "marij_law": "medical"
    },
    "MN": {
        "candidate": "sanders",
        "marij_law": "medical"
    },
    "MT": {
        "candidate": "none",
        "marij_law": "medical"
    },
    "CO": {
        "candidate": "sanders",
        "marij_law": "recreational"
    },
    "NM": {
        "candidate": "none",
        "marij_law": "medical"
    },
    "AZ": {
        "candidate": "none",
        "marij_law": "medical"
    },
    "CA": {
        "candidate": "none",
        "marij_law": "medical"
    },
    "NV": {
        "candidate": "clinton",
        "marij_law": "medical"
    },
    "OR": {
        "candidate": "none",
        "marij_law": "recreational"
    },
    "WA": {
        "candidate": "none",
        "marij_law": "recreational"
    },
    "AK": {
        "candidate": "none",
        "marij_law": "recreational"
    },
    "HI": {
        "candidate": "none",
        "marij_law": "medical"
    },
}
